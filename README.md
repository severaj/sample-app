#Sample app documentation


## Installation

### Before install dependencies
  * [nodejs](http://nodejs.org/)
  * [grunt-cli](http://gruntjs.com/getting-started)
  * [bowerjs](http://bower.io/)
  
_Follow install steps_
  

#Via git:


```bash
$ git clone git@bitbucket.org:severaj/sample-app.git
$ cd sample_app
```

Then you must create local.env.js in server/config/local.env.js
example content
```javascript
'use strict';

// Environment variables that grunt will set when the server starts locally. Use for your api keys, secrets, etc.
// You will need to set these on the server you deploy to.
//
// This file should not be tracked by git.

module.exports = {
  DOMAIN: 'http://localhost:9000',
  SESSION_SECRET: "sampleapp-secret",
  // Control debug level for modules using visionmedia/debug
  DEBUG: ''
};
```

then

```bash
$ npm install 
$ bower install
```

## Start deamon

```bash
$ grunt daemon:start
```
_server then running on port 8080_
Proxy this with NGINX for example or access it directly

## Stop deamon

```bash
$ grunt daemon:stop
```

## Api documentation
[apiary](http://docs.sampleapp1.apiary.io/)

## License

MIT