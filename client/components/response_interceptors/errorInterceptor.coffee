'use strict';
# global response interceptor, error handling
angular.module 'sampleAppApp'
.factory 'errorInterceptor', ($q, $rootScope) ->
  responseError: (rej) ->
    # brodcast error to application, is catched in rootController
    $rootScope.$broadcast '#requestError', message: "Server request error! Status #{rej.status}"
    # propagate error next to promise system
    $q.reject rej