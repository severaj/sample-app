'use strict'
# this is ui-router definition file, all ui section have this router config files
angular.module 'sampleAppApp'
.config ($stateProvider) ->
  $stateProvider
  # main state
  .state 'main',
  	# handle this part of url
    url: '/'
    templateUrl: 'app/main/main.html'
    controller: 'MainCtrl',
    resolve: 
    	# resolve prime numbers from server, if response crashed then catch rejection of promisse and return null, this state must be resolved
    	primeNumbers: ['$http', 'config', ($http, config) -> $http.get(config.apiEntryPoint).then((res) -> res.data).catch((res) -> null) ]
