'use strict';

// Number api router

var express = require('express');

var controller = require('./prime_number.controller');

var router = express.Router();

router.get('/', controller.getList);
router.get('/:number', controller.getItem);

module.exports = router;