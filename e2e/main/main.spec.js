'use strict';

describe('Main View', function() {
  var page;

  beforeEach(function() {
    browser.get('/');
    page = require('./main.po');
  });

  it('should include jumbotron with correct data', function() {
    expect(page.h1El.getText()).toBe('Sample app !');
    expect(page.perex.getText()).toBe('Prime numbers viewer');
    expect(page.bitbucketLink.getAttribute('href')).toBe('https://bitbucket.org/severaj/sample-app');
    expect(page.apiaryLink.getAttribute('href')).toBe('http://docs.sampleapp1.apiary.io/');
  });
});
